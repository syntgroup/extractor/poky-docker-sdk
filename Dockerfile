FROM debian:12

run apt-get update \
	&& apt-get install --no-install-recommends -y \
	ca-certificates \
	chrpath \
	cpio \
	debianutils \
	diffstat \
	file \
	gawk \
	git \
	git-lfs \
	liblz4-tool \
	locales \
	python3 \
	python3-git \
	socat \
	texinfo \
	unzip \
	wget \
	xz-utils \
	zstd \
	&& locale-gen en_US.UTF-8 \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

workdir /tmp
run git clone https://gitlab.com/syntgroup/extractor/docker-builder.git \
	&& cd docker-builder \
	&& git lfs pull \
	&& chmod +x ./poky-glibc-x86_64-meta-toolchain-qt5-cortexa7t2hf-neon-vfpv4-raspberrypi3-toolchain-3.1.13.sh \
	&& sh ./poky-glibc-x86_64-meta-toolchain-qt5-cortexa7t2hf-neon-vfpv4-raspberrypi3-toolchain-3.1.13.sh -y -D \
	&& cd ../ \
	&& rm -rf ./docker-builder

